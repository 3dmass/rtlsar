﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ImageLoader : MonoBehaviour {

	public Texture2D defaultImage;
	public GameObject loading;
	public RectTransform rectComponent;
	public Slider sphereRotationSlider;
	//public Slider sphereVerticalSlider;

	private float rotateSpeed = 200f;

	private void Start()
	{
		loading.SetActive (false);
		sphereRotationSlider.onValueChanged.AddListener(delegate {ValueChangeCheck(); });
	}

	private void Update()
	{
		if (rectComponent != null) 
		{
			rectComponent.Rotate (0f, 0f, rotateSpeed * Time.deltaTime);
		}
	}

	void Awake()
	{

		DontDestroyOnLoad(this);

		if (FindObjectsOfType(GetType()).Length > 1)
		{
			Destroy(gameObject);
		}

	}


	public void OpenDialog () 
	{
		LoadTextureFromImagePicker.SetPopoverAutoClose(true);
		LoadTextureFromImagePicker.SetPopoverTargetRect(Screen.width/10, Screen.height/10, 300, 400);
		LoadTextureFromImagePicker.ShowPhotoLibrary(gameObject.name, "OnFinishedImagePicker");
	}
	
	void OnFinishedImagePicker (string message) 
	{
		Debug.Log ("Image picked: "+message);
		if (LoadTextureFromImagePicker.IsLoaded()) 
		{
			int width, height;
			width = LoadTextureFromImagePicker.GetLoadedTextureWidth();
			height = LoadTextureFromImagePicker.GetLoadedTextureHeight ();
			defaultImage = LoadTextureFromImagePicker.GetLoadedTexture(message, width, height, false);
			LoadTextureFromImagePicker.ReleaseLoadedImage();

		}  
		else 
		{
			// Closed
			LoadTextureFromImagePicker.Release();
		}
		gameObject.GetComponent<Renderer> ().material.mainTexture = defaultImage;
	}

	public void GoToAR()
	{
		loading.SetActive (true);
		SceneManager.LoadSceneAsync (1);

	}

	public void ValueChangeCheck()
	{
		Quaternion rotation = Quaternion.identity;
		rotation.eulerAngles = new Vector3 (180f, sphereRotationSlider.value * 360f, 170f);
		gameObject.transform.rotation = rotation;

	}

	/*public void VerticalChangeCheck()
	{
		Quaternion rotation = Quaternion.identity;
		rotation.eulerAngles = new Vector3 (sphereVerticalSlider.value * 360f,gameObject.transform.rotation.eulerAngles.y, 180f);
		gameObject.transform.rotation = rotation;

	}*/
}
