﻿using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour {

	public Text label;
	float deltaTime = 0.0f;

	void Start()
	{
		Application.targetFrameRate = 60;

	}

	// Update is called once per frame
	void Update () 
	{
		deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
		label.text = "FPS " + (1.0f / deltaTime).ToString ("F1");
		
	}
}
