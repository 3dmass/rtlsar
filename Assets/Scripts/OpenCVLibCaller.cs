﻿using System.Runtime.InteropServices;
using System;
using UnityEngine;
using OpenCVForUnity;

public class OpenCVLibCaller : MonoBehaviour {

	public Texture2D panoImage;
	private SphereCaster caster;

	#if UNITY_IOS && !UNITY_EDITOR
	[DllImport("__Internal")]
	public static extern void helloOpenCV();
	[DllImport("__Internal")]
	public static extern IntPtr GenerateLightInfo(IntPtr panoImage, int width, int height, int length);
	#elif UNITY_EDITOR

	public void helloOpenCV()
	{
		Debug.Log ("Hello Editor");
	}

	public float[] GenerateLightInfo(IntPtr panoImage, int width, int height, int length)
	{
		Debug.Log ("Hello Editor");
		float[] array2 = new float[48] {
			0.5931615f,
			0.1658199f,
			5.83199f,
			147.2143f,
			163.1001f,
			179.4225f,
			0.3943615f,
			0.3206997f,
			8.83199f,
			147.2143f,
			163.1001f,
			179.4225f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f
		};

		/* Dummy data for desktop testing
		 *  0.5955076f,
			0.4488921f,
			12.58318f,
			147.2143f,
			163.1001f,
			179.4225f,
			0.4345038f,
			0.5819986f,
			12.26057f,
			162.6715f,
			170.6281f,
			177.1608f,
			0.791472f,
			0.8887878f,
			7.040154f,
			137.4196f,
			149.5512f,
			161.2446f,
			0.5565076f,
			0.4654973f,
			6.764836f,
			162.8021f,
			177.494f,
			191.3102f,
			0.0386453f,
			0.5007256f,
			6.564792f,
			155.7746f,
			165.789f,
			178.7154f,
			0.156181f,
			0.5848117f,
			4.880971f,
			118.3565f,
			127.4658f,
			137.4785f,
			0.1504681f,
			0.4344849f,
			1.992774f,
			118.3565f,
			127.4658f,
			137.4785f,
			0.4142165f,
			0.6747627f,
			1.730189f,
			206.5251f,
			198.3564f,
			198.8251f


			0.8862615f,
			0.6581997f,
			3.83199f,
			147.2143f,
			163.1001f,
			179.4225f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f,
			0.0f
		 * */


		//IntPtr ptrArray = Marshal.AllocHGlobal (array2.Length );
		//Marshal.Copy (array2, 0, ptrArray, array2.Length);
		return array2;
		//Marshal.FreeHGlobal (ptrArray);

	}


	#endif

	// Use this for initialization
	public void Init () 
	{

		caster = GetComponent<SphereCaster> ();
		//Size matsize = new Size ((double)panoImage.width, (double)panoImage.height);
		Mat panoMat = new Mat(panoImage.height,panoImage.width,CvType.CV_8UC3);
		Utils.texture2DToMat (panoImage, panoMat);

		byte[] byte_array= new byte[panoMat.cols () * panoMat.rows () * panoMat.channels ()];
		//copy the frame data to our array
		Utils.copyFromMat (panoMat, byte_array);
		//initialize a pointer with the apropriate size (array size)
		IntPtr pointer = Marshal.AllocHGlobal(byte_array.Length);
		Marshal.Copy (byte_array, 0, pointer , byte_array.Length);

		//Max 8 lights are supported, each light needs 6 floats worth of parameters
		float[] lighData = new float[48];  
		IntPtr ptrArray = Marshal.AllocHGlobal (lighData.Length );
		#if UNITY_IOS && !UNITY_EDITOR

		helloOpenCV ();
		ptrArray = GenerateLightInfo (pointer, panoMat.rows(), panoMat.cols(), 48);
		Marshal.Copy (ptrArray, lighData, 0, lighData.Length);
		#elif UNITY_EDITOR

		lighData = GenerateLightInfo (pointer, panoMat.cols(), panoMat.rows(), 48);

		#endif

		float coef = 0.0f;
		for (int i = 0; i < 48; i+=6) 
		{
			//Adjust height to counter spherical deformation

			if (lighData [i + 1] != 0.0f) 
			{
				coef = ((lighData [i + 1] - 0.5f) * (lighData [i + 1] - 0.5f) * (lighData [i + 1] - 0.5f)) / lighData [i + 1];
				//coef -= -0.3f;
					
			}
			Debug.Log ("[C#]Light "+(i/6).ToString()+" data: "+lighData[i].ToString()+" , "+lighData[i+1].ToString()+" , "+lighData[i+2].ToString()+" , "+lighData[i+3].ToString()+" , "+lighData[i+4].ToString()+" , "+lighData[i+5].ToString());
			//Coordinates are 1 - because: for x, the texture is flipped to achieve the mirror effect. For y because Unity is weird and has 0 BOTTOM left texcoords 
			Vector2 currentCentroid = new Vector2 (lighData [i], 1.0f - lighData [i + 1] + coef);
			Color currentColor = new Color (lighData [i+3]/255f, lighData [i + 4]/255f, lighData [i+5]/255f, 1.0f);
			if ((currentCentroid == Vector2.zero || currentCentroid == new Vector2(0.0f,1.0f)|| currentCentroid == new Vector2(0.0f,-0.3f)) && currentColor == Color.black)
				continue;
			caster.instantiateLightWithParameters (lighData [i + 2], currentColor);
			caster.putCentroid (currentCentroid);
			coef = 0.0f;

		}
		caster.setNormalizeFactor (lighData [2]);
		caster.normalizeLightIntensities ();
		//caster.placeLights ();

		Marshal.FreeHGlobal(pointer );
		Marshal.FreeHGlobal (ptrArray);

	}
		

	public void setPanoImage(Texture2D newPano)
	{
		panoImage = newPano;
	}

}
