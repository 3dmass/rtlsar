﻿using System.Collections;
using UnityEngine;
using OpenCVForUnity;

public class Cubemapper : MonoBehaviour 
{
	public Texture2D equirectangular;
	public Material GISkybox;
	public ReflectionProbe rp;
	private Texture2D[] cubemapTextures;

	public struct SkyboxManifest
	{
		public Texture2D[] textures;

		public SkyboxManifest(Texture2D front, Texture2D back, Texture2D left, Texture2D right, Texture2D up, Texture2D down)
		{
			textures = new Texture2D[6]
			{
				front,
				back,
				left,
				right,
				up,
				down
			};
		}
	}

	public static Material CreateSkyboxMaterial(SkyboxManifest manifest)
	{
		Material result = new Material(Shader.Find("RenderFX/Skybox"));
		result.SetTexture("_FrontTex", manifest.textures[0]);
		result.SetTexture("_BackTex", manifest.textures[2]);
		result.SetTexture("_LeftTex", manifest.textures[1]);
		result.SetTexture("_RightTex", manifest.textures[3]);
		result.SetTexture("_UpTex", manifest.textures[4]);
		result.SetTexture("_DownTex", manifest.textures[5]);
		return result;
	}

	void SetSkybox(Material material)
	{
		GameObject camera = this.gameObject;
		Skybox skybox = camera.GetComponent<Skybox>();
		if (skybox == null)
			skybox = camera.AddComponent<Skybox>();
		skybox.material = material;
	}

	void CreateFaces(int faceID) 
	{

		// Define our six cube faces. 
		// 0 - 3 are side faces, clockwise order
		// 4 and 5 are top and bottom, respectively
		float[,] faceTransform = new float[,]{{0f,0f},{Mathf.PI*0.5f,0f},{Mathf.PI,0f},{-Mathf.PI*0.5f,0f},{0f,-Mathf.PI*0.5f},{0f,Mathf.PI*0.5f}};
		Mat original = new Mat (equirectangular.height, equirectangular.width, CvType.CV_8UC3, new Scalar(0,0,0));
		Utils.texture2DToMat (equirectangular, original);
		float inWidth = original.cols();
		float inHeight = original.rows();
		int width = (int)(inWidth / 4f);
		int height = width;

		Mat mapx = new Mat (height, width, CvType.CV_32F);
		Mat mapy = new Mat (height, width, CvType.CV_32F);

		// Calculate adjacent (ak) and opposite (an) of the
		// triangle that is spanned from the sphere center 
		//to our cube face.
		float an = Mathf.Sin(Mathf.PI*0.25f);
		float ak = Mathf.Cos(Mathf.PI*0.25f);
		float ftu = faceTransform[faceID,0];
		float ftv = faceTransform[faceID,1];

		double[] datax = new double[1]{0};
		double[] datay = new double[1]{0};

		for (int y = 0; y < height; ++y) 
		{
			for (int x = 0; x < width; ++x) 
			{
				// Map face pixel coordinates to [-1, 1] on plane
				float nx = (float)y / ((float)height - 0.5f);
				float ny = (float)x / ((float)width - 0.5f);

				nx *= 2f;
				ny *= 2f;

				// Map [-1, 1] plane coords to [-an, an]
				// thats the coordinates in respect to a unit sphere 
				// that contains our box. 
				nx *= an; 
				ny *= an; 

				float u, v;

				// Project from plane to sphere surface.
				if(ftv == 0) {
					// Center faces
					u = Mathf.Atan2(nx, ak);
					v = Mathf.Atan2(ny * Mathf.Cos(u), ak);
					u += ftu; 
				} 
				else if(ftv > 0) 
				{ 
					// Bottom face 
					float d = Mathf.Sqrt(nx * nx + ny * ny);
					v = Mathf.PI*0.5f - Mathf.Atan2(d, ak);
					u = Mathf.Atan2(ny, nx);
				} 
				else 
				{
					// Top face
					float d = Mathf.Sqrt(nx * nx + ny * ny);
					v = -Mathf.PI*0.5f + Mathf.Atan2(d, ak);
					u = Mathf.Atan2(-ny, nx);
				}

				// Map from angular coordinates to [-1, 1], respectively.
				u = u / (Mathf.PI); 
				v = v / (Mathf.PI*0.5f);

				// Warp around, if our coordinates are out of bounds. 
				while (v < -1) 
				{
					v += 2;
					u += 1;
				} 
				while (v > 1) 
				{
					v -= 2;
					u += 1;
				} 

				while(u < -1) 
				{
					u += 2;
				}
				while(u > 1) 
				{
					u -= 2;
				}

				// Map from [-1, 1] to in texture space
				u = u / 2.0f + 0.5f;
				v = v / 2.0f + 0.5f;

				u = u * (inWidth - 1);
				v = v * (inHeight - 1);

				datax[0] = u;
				datay[0] = v;
				mapx.put (x, y, datax);
				mapy.put (x, y, datay);

			}
		}

		Mat face = new Mat (height, width, original.type());
		Imgproc.remap (original, face, mapx, mapy, Imgproc.INTER_LINEAR);

		cubemapTextures[faceID] = new Texture2D (width, height, TextureFormat.RGBA32, false);
		Utils.matToTexture2D (face, cubemapTextures[faceID]);

	}

	void Start()
	{
		cubemapTextures = new Texture2D[6];
		for (int i = 0; i < 6; ++i) 
		{
			CreateFaces (i);
		}
		SkyboxManifest manifest = new SkyboxManifest(cubemapTextures[0], cubemapTextures[1], cubemapTextures[2], cubemapTextures[3], cubemapTextures[4], cubemapTextures[5]);
		GISkybox = CreateSkyboxMaterial(manifest);
		SetSkybox(GISkybox);
		RenderSettings.skybox = GISkybox;
		rp.RenderProbe ();

	}


}

