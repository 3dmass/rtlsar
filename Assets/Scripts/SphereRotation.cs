﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereRotation : MonoBehaviour {

	public Transform referenceObject;
	private ImageLoader imgLoader;

	// Use this for initialization
	void Awake () 
	{
		//Input.compass.enabled = true;
		//Input.location.Start ();
		Vector3 targetPosition = referenceObject.position;
		gameObject.transform.position = targetPosition;
		Quaternion targetRotation = Quaternion.identity;

		if (GameObject.FindObjectOfType<ImageLoader> ()) 
		{

			imgLoader = GameObject.FindObjectOfType<ImageLoader> ().GetComponent<ImageLoader> ();
			Vector3 invertedRotation = imgLoader.transform.rotation.eulerAngles;
			//Debug.Log ("Sphere rotation: " + invertedRotation.ToString ());
			//invertedRotation.y += 180f;
			targetRotation.eulerAngles = invertedRotation;
			gameObject.transform.rotation = targetRotation;
			//gameObject.transform.Rotate (Vector3.up, 180f);

		} else 
		{
			targetRotation = Quaternion.Euler (gameObject.transform.rotation.eulerAngles.x, gameObject.transform.rotation.eulerAngles.y + Input.compass.trueHeading + 180f, gameObject.transform.rotation.eulerAngles.z);
			gameObject.transform.rotation = targetRotation;

		}

	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
