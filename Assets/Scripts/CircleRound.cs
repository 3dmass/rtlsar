﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OpenCVForUnity;

public class CircleRound : MonoBehaviour {

	public Texture2D original;
	public Texture2D detectedLights;
	public OpenCVLibCaller cvCaller;
	public SphereCaster caster;
	public Renderer ARModel;

	private ImageLoader imgLoader;

	private float timer = 0.0f;
	private bool countTimer = true;

	void Awake()
	{

		StartCoroutine ("startTimer");

		#if UNITY_IOS && !UNITY_EDITOR
		imgLoader = FindObjectOfType<ImageLoader> ().GetComponent<ImageLoader> ();
		#endif
		if (imgLoader != null) 
		{
			original = imgLoader.defaultImage;
			this.gameObject.transform.rotation = imgLoader.gameObject.transform.rotation;
			imgLoader.gameObject.SetActive(false);
		}
		caster.setEquirectangularImage (original);
		cvCaller.setPanoImage (original);
		cvCaller.Init ();

		double[] data = new double[3]{ 255, 255, 255};
		detectedLights = new Texture2D (original.width, original.height, TextureFormat.RGBA32, false);
		Mat dst = new Mat (original.height, original.width, CvType.CV_8UC3, new Scalar(0,0,0));

		for (int i = 0; i < caster.centroids.Count; ++i) 
		{
			int currentRow = (int)((1f - caster.centroids[i].y) * original.height);
			int currentCol = (int)(caster.centroids[i].x * original.width);
			for(int h = currentRow-80;h<currentRow+80;++h)
			{
				for (int w = currentCol - 80; w < currentCol + 80; ++w) 
				{
					Mathf.Clamp (w, 0, original.width);
					Mathf.Clamp (h, 0, original.height);
					dst.put (h, w, data);
					dst.put (h, w, data);
				}

			}

		}

		Mat gray = new Mat (original.height, original.width, CvType.CV_8UC3, new Scalar(0,0,0));
		Utils.texture2DToMat (original, gray);
		Imgproc.cvtColor (gray, gray, Imgproc.COLOR_RGBA2GRAY);
		Debug.Log ("[C#]Mean: " + Core.mean (gray).ToString ());
		float ambient = Mathf.Clamp( (float)Core.mean(gray).val[0] / 150f, 0.35f, 0.70f);
		for (int i = 0; i < ARModel.materials.Length; ++i) 
		{
			if((ARModel.materials[i].shader == Shader.Find("Standard")) && (ARModel.materials[i].color == Color.white))
				ARModel.materials[i].SetColor("_Color", new Color(ambient*0.5f,ambient*0.5f,ambient*0.5f));

		}



		Mat flipped = new Mat (original.height, original.width, CvType.CV_8UC3, new Scalar(0,0,0));
		Core.flip (dst, flipped, -1);
		Utils.matToTexture2D (dst, detectedLights);
		//Utils.matToTexture2D (flipped, detectedLights);

		gameObject.GetComponent<Renderer> ().material.mainTexture = detectedLights;
		caster.placeLights ();
		Debug.Log ("Pre-calculations finished after "+timer.ToString("F4")+" seconds.");
		countTimer = false;
		 
	}

	IEnumerator startTimer()
	{
		while (countTimer) 
		{
			timer += Time.deltaTime;
			yield return false;
		}
	}
		
}
