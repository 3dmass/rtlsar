﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModelCarousel : MonoBehaviour {

	private int currentModel = 0;
	public List<GameObject> arModels;

	public void nextModel()
	{
		for(int m = 0; m<arModels.Count;++m)
		{
			SetLayerRecursively (arModels [m], 0);
			  //arModels[m].layer = 0;


		}
		SetLayerRecursively (arModels [currentModel], 10);
		currentModel++;
		if (currentModel == arModels.Count) 
		{
			currentModel = 0;
		}

	}


	void SetLayerRecursively(GameObject obj, int newLayer)
	{
		if (null == obj)
		{
			return;
		}

		obj.layer = newLayer;

		foreach (Transform child in obj.transform)
		{
			if (null == child)
			{
				continue;
			}
			SetLayerRecursively(child.gameObject, newLayer);
		}
	}
}
