﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Artoolkit;
using UnityEngine.PostProcessing;
using Colorful;
using UnityEngine.UI;

public class SphereCaster : AAREventReceiver{

	private Camera sphereCamera;

	public Renderer sphere;
	public List<Transform> lights;
	public List<Vector2> centroids;
	public Transform prototypeLight;
	public bool[] placedLights;
	public ARStaticCamera ARRoot;
	public PostProcessingProfile camProfile;
	public Texture2D eqr;
	public Material skybx;
	public ReflectionProbe rp;
	public List<Slider> sliders;
	public GameObject markerParent;



	private GameObject ARCamera;
	private GameObject BackgroundCamera;
	private float normalizeFactor;
	private List<Vector3> farPositions;

	void Awake()
	{
		sphereCamera = gameObject.GetComponent<Camera> ();
		gameObject.transform.LookAt (sphere.gameObject.transform);

	}

	public override void OnMarkerFound(ARTrackable marker)
	{
		this.GetComponent<Camera> ().enabled = false;

		if (ARRoot == null) 
		{
			ARRoot = GameObject.Find ("ARToolkit").GetComponent<ARStaticCamera> ();
		
		}
		if (ARCamera == null) 
		{
			ARRoot.GetComponent<Camera> ().enabled = false;
			ARCamera = ARRoot.transform.GetChild (0).gameObject;
			ARCamera.GetComponent<Camera> ().clearFlags = CameraClearFlags.Depth;
			ARCamera.GetComponent<Camera> ().depth = 5;
			ARCamera.gameObject.AddComponent<PostProcessingBehaviour> ();
			ARCamera.gameObject.GetComponent<PostProcessingBehaviour> ().profile = camProfile;
			//ARCamera.gameObject.AddComponent<Noise> ();
			//ARCamera.gameObject.GetComponent<Noise> ().Strength = 0.75f;
			BackgroundCamera = GameObject.Find("Video background");
			BackgroundCamera.AddComponent<Sharpen> ();
			BackgroundCamera.GetComponent<Sharpen> ().Strength = 3.0f;
			ARCamera.gameObject.AddComponent<Cubemapper> ();
			ARCamera.gameObject.GetComponent<Cubemapper> ().equirectangular = eqr;
			ARCamera.gameObject.GetComponent<Cubemapper> ().GISkybox = skybx;
			ARCamera.gameObject.GetComponent<Cubemapper> ().rp = rp;
			sphere.gameObject.SetActive (false);

			for (int i = 0; i < lights.Count; ++i) 
			{
				lights [i].parent = markerParent.transform;
			
			}
		}
	}

	public override void OnMarkerTracked(ARTrackable marker)
	{
		/*GameObject trackedObj = FindObjectOfType<ARTrackable> ().gameObject;
		if (trackedObj == null) {
			Debug.Log ("Whats up with this :(");

		} else 
		{
			DrawLine (trackedObj.transform.position, lights [0].transform.position, Color.red, 0.1f);
		}*/

	}

	public override void OnMarkerLost(ARTrackable marker){
	}

	public bool areAllLightsPlaced()
	{
		for (int i = 0; i < lights.Count; ++i) 
		{
			if (placedLights [i] == false)
				return false;
			/*else 
			{
				lights [i].parent = markerParent.transform;
			}*/
		}
		return true;
	}

	public void placeLights()
	{
		Debug.Log ("Placing lights: "+centroids.Count.ToString());
		farPositions = new List<Vector3> ();
		placedLights = new bool[lights.Count];
		/*for (int j = 0; j < lights.Count; ++j)
		{
			placedLights [j] = false;
			sliders [j].gameObject.SetActive (true);
		}*/
		RaycastHit hit;

		if (sphereCamera == null) 
		{
			sphereCamera = gameObject.GetComponent<Camera> ();
			gameObject.transform.LookAt (sphere.gameObject.transform);

		}

		for (int i = 0; i < 4; ++i) 
		{
			for (int y = 0; y < sphereCamera.pixelHeight; ++y) 
			{
				for (int x = 0; x < sphereCamera.pixelWidth; ++x) 
				{
					if (!areAllLightsPlaced ()) 
					{
						Ray ray = sphereCamera.ScreenPointToRay (new Vector2 (x, y));
						//Ray ray = sphereCamera.ViewportPointToRay(new Vector3(x, y, 0));
						//Debug.DrawRay (ray.origin, ray.direction, Color.green, 10);
						if (Physics.Raycast (ray, out hit)) {
							GameObject panoSphere = hit.collider.gameObject;
							Texture2D sphereTexture = panoSphere.GetComponent<Renderer> ().material.mainTexture as Texture2D;
							/*if (sphereTexture == null) {
								Debug.Log ("Texture says its null, object is: " + panoSphere.name);

							} 
							else 
							{
								Debug.Log ("Texture says its there, name is: " + sphereTexture.name);
							}*/
							Color colorAtCollision = sphereTexture.GetPixelBilinear (hit.textureCoord.x, hit.textureCoord.y);
							//Debug.Log ("Sphere hit, color is " + colorAtCollision.ToString ());
				
							for (int uv = 0; uv < centroids.Count; ++uv) 
							{
								if (placedLights [uv])
									continue;
								if (colorAtCollision == Color.white) 
								{
									//Debug.Log ("Color is white at "+hit.textureCoord.ToString("F4"));
									if (Mathf.Abs (hit.textureCoord.x - centroids [uv].x) < 0.01f && Mathf.Abs (hit.textureCoord.y - centroids [uv].y) < 0.01f) 
 {									//if (hit.textureCoord == centroids[uv]) 
										//Debug.Log ("Texcoords: " + Mathf.Abs(hit.textureCoord.x - centroids [uv].x).ToString ("F4"));
										Vector3 lightPos = -2 * Vector3.Dot (hit.normal, ray.direction) * hit.normal + ray.direction;
										lightPos = lightPos.normalized;
										//lightPos.y = Mathf.Abs (lightPos.y);
										if(i == 0 || i == 2)
											lightPos.z *= -1f;
										else 
											lightPos.x *= -1f;
										lightPos.y = Mathf.Clamp (Mathf.Abs (lightPos.y), 0.7f, 1.0f);
										if (centroids [uv].y > 0.65f)
											lightPos.y = 0.95f;
										lightPos = lightPos.normalized;
										//lightPos *= 10f;
										lights [uv].position = lightPos;
										lights [uv].transform.LookAt (markerParent.transform);
										placedLights [uv] = true;
										farPositions.Add (lightPos);
										Debug.Log ("Light " + uv.ToString () + " placed at " + lightPos.ToString ());
										//Debug.DrawRay (hit.point, lightPos - hit.point, Color.red, 10);
										//continue;
									}
								}

							}

						}
					}

				}
			}
			gameObject.transform.RotateAround (sphere.transform.position, Vector3.up, 90f);
			//gameObject.transform.LookAt (sphere.gameObject.transform);
		}
		if (!areAllLightsPlaced()) 
		{
			for (int i = 0; i < lights.Count; ++i) 
			{
				if (placedLights [i] == false) 
				{

					lights [i].GetComponent<Light> ().intensity = 0.0f;

				}
				
			}
		}
	}

	public void instantiateLightWithParameters(float intensity, Color color)
	{
		Transform oneMore = Instantiate (prototypeLight);
		oneMore.gameObject.GetComponent<Light> ().intensity = intensity;
		color.r = Mathf.Clamp (color.r, 0f, 0.8f);
		color.g = Mathf.Clamp (color.g, 0f, 0.8f);
		color.b = Mathf.Clamp (color.b, 0f, 0.8f);
		oneMore.gameObject.GetComponent<Light>().color = color;
		lights.Add (oneMore);

	}

	public void putCentroid(Vector2 newCentroid)
	{
		centroids.Add (newCentroid);

	}

	public void setNormalizeFactor(float brightest)
	{
		if (lights.Count > 1) 
		{

			normalizeFactor = 1.0f / (brightest*1.43f);
		} 
		else 
		{

			normalizeFactor = 1.0f;
		}

		Debug.Log ("Normalization factor: "+normalizeFactor.ToString("F4"));
	}

	public void normalizeLightIntensities()
	{

		for (int i = 0; i < lights.Count; ++i) 
		{
			lights [i].GetComponent<Light> ().intensity *= normalizeFactor;
			lights [i].GetComponent<Light> ().intensity = Mathf.Clamp (lights [i].GetComponent<Light> ().intensity, 0.0f, 0.7f);
			lights [i].GetComponent<Light> ().shadowStrength = lights [i].GetComponent<Light> ().intensity * 0.9f;
		}

	}

	/*void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.2f)
	{
		GameObject myLine = new GameObject();
		myLine.layer = LayerMask.NameToLayer("ARContent");
		myLine.transform.position = start;
		myLine.AddComponent<LineRenderer>();
		LineRenderer lr = myLine.GetComponent<LineRenderer>();
		lr.material = new Material(Shader.Find("Unlit/Color"));
		lr.startColor = color;
		lr.endColor = color;
		lr.SetWidth(0.01f, 0.01f);
		lr.SetPosition(0, start);
		lr.SetPosition(1, end);
		GameObject.Destroy(myLine, duration);
	}*/

	public void setEquirectangularImage(Texture2D newEqr)
	{
		eqr = newEqr;

	}

	public void ValueChangeCheck(int id)
	{
		Vector3 updPos = farPositions[id];
		updPos *= sliders [id].value;
		lights [id].transform.position = updPos;

	}

}
