﻿Shader "Custom/TransparentShadow" 
{
Properties 
{
         _ActiveLights("Active Lights", Range(0,10)) = 1
     }
     
     SubShader {
         Tags {"Queue"="AlphaTest" "RenderType"="TransparentCutout"}
         LOD 150
         Blend Zero SrcColor
         Offset 0, -1
         ZWrite On
     
     CGPROGRAM
     
     #pragma surface surf ShadowOnly alphatest:_Cutoff fullforwardshadows
     #include "UnityCG.cginc"
     #include "AutoLight.cginc"
         
     float _ActiveLights;
     
     struct Input 
     {
         float4 pos : SV_POSITION;
     };

     inline fixed4 LightingShadowOnly (SurfaceOutput s, fixed3 lightDir, fixed atten) {
         fixed4 c;
       
         c.rgb = s.Albedo*atten;
         c.a = s.Alpha;
         return c;
     }
     
     void surf (Input IN, inout SurfaceOutput o) 
     {
         //fixed4 c = (_LightColor0 + _LightColor0 * _Color)*(1.0f/_ActiveLights);
         fixed4 c = _LightColor0*(1.0f/_ActiveLights);
         o.Albedo = c.rgb;
         o.Alpha = 1.0f;
     }
     ENDCG
     }
     Fallback "Transparent/Cutout/VertexLit"
} 